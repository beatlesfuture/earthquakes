package com.blealtesfuture.earthquakes.root

import android.app.Application
import com.blealtesfuture.earthquakes.request.RequestEarthquakeModule

class App : Application() {
    var component : ApplicationComponent? = null

    override fun onCreate() {
        super.onCreate()

        component = DaggerApplicationComponent.builder()
            .appModule(AppModule(this))
            .requestEarthquakeModule(RequestEarthquakeModule())
            .build()

        component?.inject(this)
    }
}