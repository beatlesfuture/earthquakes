package com.blealtesfuture.earthquakes.root

import com.blealtesfuture.earthquakes.request.RequestEarthquakeModule
import com.blealtesfuture.earthquakes.view.MainContainer
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, RequestEarthquakeModule::class))
interface ApplicationComponent {
    fun inject(app : App)
    fun inject(mainContainer: MainContainer)
}