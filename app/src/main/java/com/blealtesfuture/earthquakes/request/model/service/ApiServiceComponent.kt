package com.blealtesfuture.earthquakes.request.model.service

import com.blealtesfuture.earthquakes.request.model.RequestModelImp
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf( ServiceModule::class))
interface ApiServiceComponent {
    fun inject(modelImp : RequestModelImp)
}