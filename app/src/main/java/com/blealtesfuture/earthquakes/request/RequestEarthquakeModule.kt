package com.blealtesfuture.earthquakes.request

import com.blealtesfuture.earthquakes.request.model.RequestModelImp
import com.blealtesfuture.earthquakes.request.presenter.RequestPresenterImp
import dagger.Module
import dagger.Provides

@Module
class RequestEarthquakeModule {
    @Provides
    fun providesPresenter(model: RequestEarthquakesMVP.Model) : RequestEarthquakesMVP.Presenter{
        return RequestPresenterImp(model)
    }

    @Provides
    fun providesModel(): RequestEarthquakesMVP.Model{
        return RequestModelImp()
    }
}