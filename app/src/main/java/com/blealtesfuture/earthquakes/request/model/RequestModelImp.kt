package com.blealtesfuture.earthquakes.request.model

import com.blealtesfuture.earthquakes.request.RequestEarthquakesMVP
import com.blealtesfuture.earthquakes.request.model.service.ApiService
import com.blealtesfuture.earthquakes.request.model.service.DaggerApiServiceComponent
import com.blealtesfuture.earthquakes.request.model.service.ServiceModule
import com.blealtesfuture.earthquakes.request.model.service.response.Root
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class RequestModelImp() : RequestEarthquakesMVP.Model {
    private var presenter : RequestEarthquakesMVP.Presenter? = null
    init {

        DaggerApiServiceComponent.builder()
            .serviceModule(ServiceModule())
            .build()
            .inject(this)

    }
    @Inject
    lateinit var service : ApiService

    override fun setPrersenter(presenter: RequestEarthquakesMVP.Presenter) {
       this.presenter = presenter
    }

    override fun requestEarthEvents(startTime: String, endTime: String, minMagnitude: Double) {
        CoroutineScope(Dispatchers.IO).launch {
            val call = service.searchEarthquakes( "geojson",  startTime, endTime, "" + minMagnitude)

            call.enqueue(object : Callback<Root> {
                override fun onFailure(call: Call<Root>, t: Throwable) {
                    presenter?.showServiceError(t.message?:"")
                }

                override fun onResponse(call: Call<Root>, serviceResponse: Response<Root>) {
                    if(serviceResponse.errorBody() == null) {
                        presenter?.showResponse(serviceResponse.body()!!.features)
                    }else{
                        presenter?.showServiceError(serviceResponse.message())
                    }
                }

            })
        }
    }

}