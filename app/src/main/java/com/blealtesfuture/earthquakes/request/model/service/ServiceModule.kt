package com.blealtesfuture.earthquakes.request.model.service

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class ServiceModule {

    private val BASE_URL = "https://earthquake.usgs.gov/"

    @Provides
    fun providesRetorfit() : Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun providesService() : ApiService {
        return  providesRetorfit().create(ApiService::class.java)
    }
}