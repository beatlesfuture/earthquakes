package com.blealtesfuture.earthquakes.request.model.service

import com.blealtesfuture.earthquakes.request.model.service.response.Feature
import com.blealtesfuture.earthquakes.request.model.service.response.Root
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("fdsnws/event/1/query?")
    fun searchEarthquakes( @Query("format") format: String, @Query("starttime") startTime : String, @Query("endtime") endTime : String, @Query("minmagnitude") magnitude : String) : Call<Root>
}