package com.blealtesfuture.earthquakes.request.model.persistence

import android.content.Context
import android.content.SharedPreferences

class StoreLastSearch(val context: Context) {


    val FILE_NAME = "FileLastSearch"
    val STAR_TIME = "com.blealtesfuture.earthquakes.request.model.persistence.START_TIME"
    val END_TIME = "com.blealtesfuture.earthquakes.request.model.persistence.END_TIME"
    val MAGNITUDE = "com.blealtesfuture.earthquakes.request.model.persistence.MAGNITUDE"
    var sharedPreferences: SharedPreferences? = null
    private var editor: SharedPreferences.Editor? = null


    init {
        sharedPreferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE)
        editor = sharedPreferences?.edit()
    }


    fun setStarTime(value: String) {
        editor?.putString(STAR_TIME, value)
        editor?.commit()
    }

    fun getStarTime(): String {
        return sharedPreferences?.getString(STAR_TIME, "")?:""
    }

    fun setEndTime(value: String) {
        editor?.putString(END_TIME, value)
        editor?.commit()
    }

    fun getEndTime(): String {
        return sharedPreferences?.getString(END_TIME, "")?:""
    }

    fun setMagnitud(value: String) {
        editor?.putString(MAGNITUDE, value)
        editor?.commit()
    }

    fun getMagnitude(): String {
        return sharedPreferences?.getString(MAGNITUDE, "")?:""
    }
}