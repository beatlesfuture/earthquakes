package com.blealtesfuture.earthquakes.request.presenter

import com.blealtesfuture.earthquakes.request.RequestEarthquakesMVP
import com.blealtesfuture.earthquakes.request.model.service.response.Feature
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class RequestPresenterImp(val model : RequestEarthquakesMVP.Model) : RequestEarthquakesMVP.Presenter {

    private var view : RequestEarthquakesMVP.View? = null

    init {
        model.setPrersenter(this)
    }
    override fun setView(view: RequestEarthquakesMVP.View) {
       this.view = view
    }

    override fun requestEarthEvents(startTime: String, endTime: String, minMagnitude: Double) {

        if (!correctDate(startTime) || !correctDate(endTime) ){
            view?.showDateIncorrect()
        }

        if (minMagnitude < 0 ){
            view?.showMagnitudeError()
            return
        }
        model.requestEarthEvents(startTime, endTime, minMagnitude)
    }

    override fun showServiceError(msg: String) {
        view?.showServiceError(msg)
    }

    override fun showResponse(arryList: List<Feature>) {
       view?.showResponse(arryList)
    }


    fun correctDate(dateStr: String) : Boolean{
        try {
            var formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val date = formatter.parse(dateStr)
            return true
        }catch (e : ParseException){
            return false;
        }
    }
}