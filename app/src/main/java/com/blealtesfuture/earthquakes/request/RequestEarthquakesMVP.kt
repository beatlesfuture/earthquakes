package com.blealtesfuture.earthquakes.request

import com.blealtesfuture.earthquakes.request.model.service.response.Feature

interface RequestEarthquakesMVP {

    interface View {
        fun showResponse(arryList: List<Feature>)
        fun showServiceError(msg: String)
        fun showMagnitudeError()
        fun showDateIncorrect()

    }

    interface Presenter{
        fun setView (view: View)

        fun requestEarthEvents(startTime: String, endTime: String, minMagnitude: Double)
        fun showServiceError(msg: String)
        fun showResponse(arryList: List<Feature>)

    }

    interface Model{
        fun setPrersenter(presenter: Presenter)

        fun requestEarthEvents(startTime: String, endTime: String, minMagnitude: Double)

    }

}