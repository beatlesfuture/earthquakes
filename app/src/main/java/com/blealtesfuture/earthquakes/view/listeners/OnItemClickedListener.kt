package com.blealtesfuture.earthquakes.view.listeners

import com.blealtesfuture.earthquakes.request.model.service.response.Properties

interface OnItemClickedListener {
    fun onItemClicked(earthquakePropertie : Properties)
}