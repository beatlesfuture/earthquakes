package com.blealtesfuture.earthquakes.view.adapter

import android.content.Context
import android.os.Build
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.blealtesfuture.earthquakes.R
import com.blealtesfuture.earthquakes.request.model.service.response.Feature
import com.blealtesfuture.earthquakes.view.listeners.OnItemClickedListener

class EarthquakesAdapter(
    private var responseList: List<Feature>?,
    val callback: OnItemClickedListener,
    val context: Context
): RecyclerView.Adapter<EarthquakesAdapter.EarthquakeViewholder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EarthquakeViewholder {
        val v = LayoutInflater.from(parent.context).inflate(
            R.layout.earthquake_itme_layout,
            parent,
            false
        )
        return EarthquakeViewholder(v)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: EarthquakeViewholder, position: Int) {
        val propertie = responseList?.get(position)?.properties
        holder.tvPlace.text = propertie?.place
        holder.tvMagnitude.text = propertie?.mag.toString()
        val timeSec: Long = 84561 // Json output

        holder.tvHour.text = DateUtils.getRelativeTimeSpanString(propertie?.time?.toLong() ?: 0)

        setMagnitudColor(holder.crdContainer, propertie?.mag ?: 0.0)
        holder.crdContainer.setOnClickListener(View.OnClickListener {
            callback.onItemClicked(propertie!!)
        })
    }

    override fun getItemCount(): Int {
        return responseList?.size ?: 0
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun setMagnitudColor(crdContainer: CardView, magnitud: Double){
        when (magnitud){
            in 0.0..4.0 -> {
                crdContainer.setBackgroundColor(context.getColor(R.color.greeen))
            }
            in 4.0..6.0 -> {
                crdContainer.setBackgroundColor(context.getColor(R.color.yellow))
            }
            in 6.0..7.0 -> {
                crdContainer.setBackgroundColor(context.getColor(R.color.orage))
            }
            else -> {
                crdContainer.setBackgroundColor(context.getColor(R.color.red))
            }
        }
    }

    class EarthquakeViewholder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvPlace = itemView.findViewById<TextView>(R.id.place)
        var tvMagnitude = itemView.findViewById<TextView>(R.id.magnitude)
        var tvHour = itemView.findViewById<TextView>(R.id.hour)
        var crdContainer = itemView.findViewById<CardView>(R.id.crdContainer)

    }

}