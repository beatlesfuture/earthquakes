package com.blealtesfuture.earthquakes.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blealtesfuture.earthquakes.R
import com.blealtesfuture.earthquakes.request.RequestEarthquakesMVP
import com.blealtesfuture.earthquakes.request.model.persistence.StoreLastSearch
import com.blealtesfuture.earthquakes.request.model.service.response.Feature
import com.blealtesfuture.earthquakes.request.model.service.response.Properties
import com.blealtesfuture.earthquakes.view.adapter.EarthquakesAdapter
import com.blealtesfuture.earthquakes.view.listeners.OnItemClickedListener

class FormFragment(val presenter: RequestEarthquakesMVP.Presenter) : Fragment(), View.OnClickListener, OnItemClickedListener {

    private lateinit var btnSearch : Button
    private lateinit var btnSearchLast : Button
    private var rvEarthquakes : RecyclerView? = null
    private var store : StoreLastSearch? = null

    private lateinit var etxStarTime : EditText
    private lateinit var etxEndTime : EditText
    private lateinit var etxMagnitude : EditText

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.form_fragment, container, false)

        btnSearch = view.findViewById(R.id.btnSearch)
        btnSearchLast = view.findViewById(R.id.btnSearchLast)
        rvEarthquakes = view.findViewById(R.id.rvEarthquakes)

        etxStarTime = view.findViewById(R.id.etxStarTime)
        etxEndTime = view.findViewById(R.id.etxEndTime)
        etxMagnitude = view.findViewById(R.id.etxMagnitude)


        store = StoreLastSearch(context!!)

        btnSearchLast.setOnClickListener(this)
        btnSearch.setOnClickListener(this)

        rvEarthquakes?.layoutManager = LinearLayoutManager(activity)
        rvEarthquakes?.isMotionEventSplittingEnabled = false
        rvEarthquakes?.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.HORIZONTAL))



        return view
    }

    override fun onClick(p0: View?) {
        when(p0?.id){
            R.id.btnSearch ->{
                val startTime = etxStarTime.text.toString()
                val endTime = etxEndTime.text.toString()
                val magnitude = etxMagnitude.text.toString().toDouble()

                presenter.requestEarthEvents(startTime, endTime, magnitude)
                store?.setStarTime(startTime)
                store?.setEndTime(endTime)
                store?.setMagnitud(magnitude.toString())


            }

            R.id.btnSearchLast ->{
                //solicitando los datos de la ultima búsqueda realizada
                presenter.requestEarthEvents(store?.getStarTime()?:"", store?.getEndTime()?:"", (store?.getMagnitude()?:"0").toDouble())

            }

        }
    }

    fun showEathquakes(responseList :List<Feature>){
        rvEarthquakes?.adapter = EarthquakesAdapter(responseList, this, activity!!.applicationContext)
    }

    override fun onItemClicked(earthquakePropertie: Properties) {
        Log.d("FormFragment", "tocado")
    }
}