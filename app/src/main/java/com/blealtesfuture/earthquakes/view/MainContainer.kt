package com.blealtesfuture.earthquakes.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.blealtesfuture.earthquakes.R
import com.blealtesfuture.earthquakes.request.RequestEarthquakesMVP
import com.blealtesfuture.earthquakes.request.model.persistence.StoreLastSearch
import com.blealtesfuture.earthquakes.request.model.service.response.Feature
import com.blealtesfuture.earthquakes.root.App
import javax.inject.Inject

class MainContainer : AppCompatActivity(), RequestEarthquakesMVP.View{
    @Inject
    lateinit var presenter: RequestEarthquakesMVP.Presenter

    lateinit var formFragment : FormFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_container_activity)

        (application as App).component?.inject(this)
        presenter.setView(this)

        formFragment = FormFragment(presenter)
        changeFragment(formFragment)


    }


    private fun changeFragment(currentFragment : Fragment){
        val manager: FragmentManager = supportFragmentManager
        val transaction: FragmentTransaction = manager.beginTransaction()

        //checando si un fragment ya existe, si es correcto no es necesario volver a crearlo
        if (manager.findFragmentByTag(currentFragment.javaClass.canonicalName) == null) {
            transaction.replace(R.id.frmContainer, currentFragment)
            if(!(currentFragment is FormFragment)) {
                transaction.addToBackStack("backStack")
            }
            transaction.commitAllowingStateLoss()
        }
    }

    override fun showResponse(arryList: List<Feature>) {
        formFragment.showEathquakes(arryList)
    }


    override fun showServiceError(msg: String) {
       showMsg(getString(R.string.service_error, msg))
    }


    override fun showMagnitudeError() {
        showMsg(getString(R.string.magnitude_error_msg))
    }

    override fun showDateIncorrect() {
        showMsg(getString(R.string.incorrect_date_format))
    }


    private fun showMsg(msg : String ){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}